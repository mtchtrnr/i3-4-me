Put `config` into `~/.config/i3/`

and `HopperHouse.png` into `/usr/share/backgrounds/`

Install:

**i3**

```sh
apt update
apt install i3
```
**i3-gaps**
```
sudo add-apt-repository ppa:regolith-linux/release
sudo apt update
sudo apt install i3-gaps
```
**feh**
```
sudo apt install feh
```

Logout and choose i3 as your thing.

---  

If things go awry:

Logout with
```
i3-msg exit
```

Reset i3 with `mod + shift + 'r'`
